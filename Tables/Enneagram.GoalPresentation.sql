CREATE TABLE [Enneagram].[GoalPresentation]
(
[GoalPresentationId] [int] NOT NULL IDENTITY(1, 1),
[Value] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
