CREATE TABLE [DISC].[Results]
(
[ResultId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Profile] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Results_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
