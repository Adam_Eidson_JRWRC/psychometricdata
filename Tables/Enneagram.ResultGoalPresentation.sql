CREATE TABLE [Enneagram].[ResultGoalPresentation]
(
[ResultGoalPresentation] [int] NOT NULL IDENTITY(1, 1),
[GoalPresentationId] [int] NOT NULL,
[ResultId] [int] NOT NULL
) ON [PRIMARY]
GO
