CREATE TABLE [Enneagram].[Results]
(
[ResultId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConflictStrategy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnneagramType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnneagramTypeAKA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GoalPresentation] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instinct] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DominantCenter] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WeakestCenter] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LevelOfIntegration] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MetaMessage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OverallStrain] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnvironmentalStrain] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VocationalStrain] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhysicalStrain] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InterpersonalStrain] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PsychologicalStrain] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HappinessLevel] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_Results_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
