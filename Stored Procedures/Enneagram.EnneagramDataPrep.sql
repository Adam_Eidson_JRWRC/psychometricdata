SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 12/4/2019
-- Description:	Cleans and prepares Enneagram data upon completion of PDF import ETL
-- =============================================
--exec Enneagram.dbo.EnneagramDataPrep 
CREATE PROCEDURE [Enneagram].[EnneagramDataPrep] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/****** Script for SelectTopNRows command from SSMS  ******/
	  update r
	  set EnneagramTypeAKA = 'Loyal Sceptic'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = 'Loyal ScepticDevotion'


	  update r
	  set EnneagramTypeAKA = 'Considerate Helper'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = ' Considerate HelperWarm'


 
	  update r
	  set EnneagramTypeAKA = 'Active Controller'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = ' Active ControllerAssertive'

 
	  update r
	  set EnneagramTypeAKA = 'Competitive Achiever'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = ' Competitive AchieverAmbitious'

  update r
	  set EnneagramTypeAKA = 'Quiet Specialist'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = 'Quiet SpecialistPerceptive'
  or EnneagramTypeAKA = ' Quiet SpecialistPerceptive'

  update r
	  set EnneagramTypeAKA = 'Strict Perfectionist'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA =' Strict PerfectionistPrincipled'
  
  update r
	  set EnneagramTypeAKA = 'Adaptive Peacemaker'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = ' Adaptive PeacemakerAgreeable'

  
  update r
	  set EnneagramTypeAKA = 'Enthusiastic Visionary'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = ' Enthusiastic VisionaryOptimistic'


  update r
	  set EnneagramTypeAKA = 'Intense Creative'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA =  'Intense CreativeSelf-Attuned'
 
  
  update r
	  set EnneagramTypeAKA = 'Intense Creative'
  FROM [PsychometricData].[Enneagram].[Results] r
  where EnneagramTypeAKA = ' Intense CreativeSelf-Attuned'
   

  update r
set instinct = LTRIM(RTRIM(SUBSTRING(instinct , charindex('with a',instinct)+6,50)))
From results r
where len(instinct)>25

	insert into GoalPresentation
	( Value)

select r.*
From 
  (select distinct LTRIM(RTRIM(Value )) val
  FROM [PsychometricData].[Enneagram].[Results]
  Cross APply STRING_SPLIT([GoalPresentation], '?')) r
  left join GoalPresentation gp on gp.value = r.val
  where r.val <> ''
  and gp.value is null
  

	insert into ResultGoalPresentation
	( ResultId,
	GoalPresentationId)

	select R.ResultId, gp.GoalPresentationId 
	FROM (select distinct ResultId, LTRIM(RTRIM(Value )) as [Value]
  FROM [PsychometricData].[Enneagram].[Results]
  Cross APply STRING_SPLIT([GoalPresentation], '?')) R 
  join Enneagram.dbo.GoalPresentation gp on gp.Value = R.Value
  left join ResultGoalPresentation rgp on rgp.resultId = R.ResultId
										and rgp.GoalPresentationId = gp.GoalPresentationId
  where R.Value <> ''
  and rgp.resultId is null

END
GO
GRANT EXECUTE ON  [Enneagram].[EnneagramDataPrep] TO [ETL_User]
GO
