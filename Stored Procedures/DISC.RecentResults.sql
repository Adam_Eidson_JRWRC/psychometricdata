SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 12/4/2019
-- Description:	Returns DISC data records created since a defined period
-- =============================================
--exec PsychometricData.DISC.[RecentResults] @CreatedInLastXHours =182
CREATE PROCEDURE [DISC].[RecentResults] 
	@CreatedInLastXHours int --number of hours to look back
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ResultId]
      ,[Name]
      ,[Profile]
      ,[CreatedDate]
  FROM [PsychometricData].DISC.[Results]
  where createddate >= dateadd(hour,-@CreatedInLastXHours,getdate())


END
GO
GRANT EXECUTE ON  [DISC].[RecentResults] TO [ETL_User]
GO
