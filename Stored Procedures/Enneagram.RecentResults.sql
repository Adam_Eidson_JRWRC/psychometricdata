SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 12/4/2019
-- Description:	Returns Enneagram data records created since a defined period
-- =============================================
--exec PsychometricData.Enneagram.[RecentResults] @CreatedInLastXHours =2000
CREATE PROCEDURE [Enneagram].[RecentResults] 
	@CreatedInLastXHours int --number of hours to look back
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ResultId]
      ,[Name]
      
      ,[EnneagramType]
      ,[EnneagramTypeAKA]
      ,[GoalPresentation]
      ,[Instinct]
      ,[DominantCenter]
      ,[WeakestCenter]
      ,[LevelOfIntegration]
      ,[MetaMessage]
      ,[OverallStrain]
      ,[EnvironmentalStrain]
      ,[VocationalStrain]
      ,[PhysicalStrain]
      ,[InterpersonalStrain]
      ,[PsychologicalStrain]
      ,[HappinessLevel]
	  ,[ConflictStrategy]
      ,[CreatedDate]
  FROM [PsychometricData].[Enneagram].[Results]
  where createddate >= dateadd(hour,-@CreatedInLastXHours,getdate())


END
GO
GRANT EXECUTE ON  [Enneagram].[RecentResults] TO [ETL_User]
GO
